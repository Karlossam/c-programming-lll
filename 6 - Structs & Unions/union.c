#include <stdio.h>

int main() {
    union myunion_u {
        int x; // 4 bytes
        char c; // 1 byte
        short s; // 2 bytes
    };

    union myunion_u u;
    u.x = 0x41424344;
    // hx es la mitad de x; mientras que hhx es un cuarto
    printf("%hx %hhx\n", u.s, u.c); 
}