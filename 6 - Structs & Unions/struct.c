#include <stdio.h>
#include <stdbool.h>

#define MAX_EMPLOYEES 1000

struct employee_t {
	int id;
	char *first_name;
	char *last_name;
	float income;
	bool ismanager;
};

int main() {
	struct employee_t Fred;
	Fred.income = 100.00;
	Fred.ismanager = false;
	Fred.first_name = "Fred";
	Fred.last_name = "Neuman";

    printf("Employee:\n");
    printf("    - Name: %s %s\n", Fred.first_name, Fred.last_name);
    printf("    - Income: %f\n", Fred.income);
    if (Fred.ismanager) {
        printf("    - Is Manager: True\n");
    } else {
        printf("    - Is Manager: False\n");
    }

    // Tambien podemos hacer un array de structs
	struct employee_t employees[MAX_EMPLOYEES];

	int i = 0;
	for (i=0; i<MAX_EMPLOYEES; i++) {
		employees[i].income = 0;
		employees[i].ismanager = false;
		//.... etc
	}
    
}