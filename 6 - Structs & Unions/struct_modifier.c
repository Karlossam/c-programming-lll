#include <stdio.h>
#include <stdbool.h>

__attribute__((__packed__)) struct employee_t {
	int id;
	char *first_name;
	char *last_name;
	float income;
	bool ismanager;
};

int main() {
    printf("Size of employee: %ld\n", sizeof(struct employee_t));
}