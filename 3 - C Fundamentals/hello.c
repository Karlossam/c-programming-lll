#include <stdio.h> // Preprocessor include. Literalmente incluye el archivo indicado en este antes de compilar

int main(){ // Definimos funcion
    printf("Hello World!\n"); // Mostramos por pantalla
    
    #ifdef DEBUG // Preprocessor ifdef. Comprueba si se ha definido la macro DEBUG. Si se ha definido incluye el print, si no no. Podemos pasarla mediante 'gcc -DDEBUG ...'
    printf("In DEBUG mode: %s:%d\n", __FILE__, __LINE__);
    #endif
    
    return 0; // Devolvemos int
}