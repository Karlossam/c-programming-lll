#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include <sys/types.h>
#include <sys/stat.h>

int main (int argc, char *argv[]) {
    // Probamos uso basico de open
    int fd = open("./a-file", O_RDWR | O_CREAT, 0644);
    if (fd == -1) {
        perror("open");
        return -1;
    }
    // Probamos write
    char *a_buf = "some data\n";
    write(fd, a_buf, strlen(a_buf));
    close(fd);

    // Probamos read con struct y todo
    int another_fd = open("./my-db.db", O_RDONLY);
    if (another_fd == -1) {
        perror("open");
        return -1;
    }

    struct database_header {
        unsigned short version;
        unsigned short employee;
        unsigned int file_length;
    };

    struct database_header head = {0};

    read(another_fd, &head, sizeof(head));
    printf("Database Version %d\n", head.version);
    printf("Database Size %ld\n", sizeof(head));

    return 1;
}