#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>

struct employee_t {
	int id;
	float income;
	bool ismanager;
};


int initialize_employee(struct employee_t *e) {
    static int numEmployee = 0;
	numEmployee++;

	e->id = numEmployee;
	e->income = 1000;
	e->ismanager = false;

	return numEmployee;
}


int main() {
    // Mediante memory allocator dinamico.
    int n = 10;
    struct employee_t *employees = malloc(sizeof(struct employee_t)*n);

    // Debemos comprobar que no sea NULL
    if (employees == NULL) {
        printf("The allocator failed\n");
        return -1;
    }

	for (int i=0; i<n; i++) {
		int id = initialize_employee(&employees[i]);
		printf("New employee ID is: %d\n", id);
	}

    // Liberamos la memoria para no crear bugs de "use after free"
    free(employees);
    employees = NULL;
}