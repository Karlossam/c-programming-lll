#include <stdio.h>
#include <stdbool.h>

struct employee_t {
	int id;
	float income;
	bool ismanager;
};


void initialize_employee(struct employee_t *e) {
	e->id = 0;
	e->income = 1000;
	e->ismanager = false;

	return;
}


int main() {
    int x = 3; // Variable normal
    int *pX = &x; // Pointer a x. 

    printf("X Value(*pX): %d\n", *pX); // Mostramos valor de x
    printf("X Address(pX): %p\n", pX); // Mostramos direccion de x

    struct employee_t Ralph;
    initialize_employee(&Ralph);
    printf("Ralph Address: %p\n", &Ralph);
}