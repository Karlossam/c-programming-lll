#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>

struct employee_t {
	int id;
	float income;
	bool ismanager;
};


void initialize_employee(struct employee_t *e) {
	e->id = 0;
	e->income = 1000;
	e->ismanager = false;

	return;
}


int main() {
    // Mediante memory allocator dinamico.
    int n = 4;
    struct employee_t *employees = malloc(sizeof(struct employee_t)*n);

    // Debemos comprobar que no sea NULL
    if (employees == NULL) {
        printf("The allocator failed\n");
        return -1;
    }

    // Comprobamos la usabilidad
    initialize_employee(&employees[0]);
    printf("%f\n", employees[0].income);

    // Liberamos la memoria para no crear bugs de "use after free"
    free(employees);
    employees = NULL;
}