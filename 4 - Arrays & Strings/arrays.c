#include <stdio.h>

#define MAX_IDS 32

int main(){
	int ids[] = {1,2,3};
    // printf(ids); // Da error al compilar porque estamos pasando un pointer a la funcion.
    printf("%d\n", ids[0]); // Mostramos el elemento 0.

	// Tambien podemos definir su tamaño
	int ids_alt[MAX_IDS] = {0,1,2};

	ids_alt[3] = 0x41; // Asignamos un Hex 
	printf("%d\n",ids_alt[3]); // Mostramos
}