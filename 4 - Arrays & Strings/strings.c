#include <stdio.h>

int main(){
	char my_str[] = {'h','e','l','l','o'}; // Esto es vulnerable porque no termina en 0!!
	char my_str_alt[] = {'t','e','s','t', 0}; // Esto ya no es vulnerable al estar null terminated
	char *myotherstr = "hello"; // Hace null termination automaticamente al usar " y crearlo mediante un pointer.
	printf("%s\n", my_str); // Mostramos por pantalla
    printf("%s\n", my_str_alt); // Mostramos por pantalla
    printf("%s\n", myotherstr); // Mostramos por pantalla
}