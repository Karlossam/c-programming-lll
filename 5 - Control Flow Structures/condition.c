#include <stdio.h>

int main(){
	int temp;
	printf("What temperature is it? ");
	scanf("%d", &temp); // No preguntes, admite input.

	if (temp >= 70) {
		printf("Bro its hot\n");
	} else if (temp >= 30 && temp < 70) { // Aqui vemos el operador AND > && y un par de comparadores
		printf("Bro its mild\n");
	} else {
		printf("Bro its cold\n");
	}
}